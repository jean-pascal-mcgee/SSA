#include <ctime>
#include <stdio.h>
#include <iostream>
#include <conio.h>
#include <vector>

#include "Objets.h"

using namespace std;

#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_LEFT 75
#define KEY_RIGHT 77
#define HAUTEUR_GRILLE 30
#define LARGEUR_GRILLE 30
#define NOMBRE_ENNEMIE 5

int main()
{
	//Teste
	Grille grilleDeJeu(HAUTEUR_GRILLE,LARGEUR_GRILLE);	
	Joueur joueur_obj(10, '^', LARGEUR_GRILLE/2,2,1);	

	vector<Ennemie*> Ennemie_Obj;
	vector<Projectile*> ProjectileJoueur;
	vector<Projectile*> ProectileEnnemies;


	while (1)
	{
		int c = 0;
		//Vider la grille et la console
		system("cls");
		grilleDeJeu.viderGrille();

		//G�n�rer les ennemies

		while (Ennemie_Obj.size() <= NOMBRE_ENNEMIE)
		{
			int xRand = (rand() % (grilleDeJeu.getLargeur() - 1) + 1);
			if (grilleDeJeu.getCase(xRand, grilleDeJeu.getHauteur() - 2)->estVide())
			{
				Ennemie_Obj.push_back(new Ennemie(1, '#', xRand, grilleDeJeu.getHauteur() - 2, 10));
			}
		}

		//R�afficher le joueur
		grilleDeJeu.getCase(joueur_obj.getX(), joueur_obj.getY())->set_image(joueur_obj.getImage());


		//Afficher Ennemies
		for (int itEnnemie = 0; itEnnemie < Ennemie_Obj.size(); itEnnemie++) 
		{
			grilleDeJeu.getCase(Ennemie_Obj.at(itEnnemie)->getX(), Ennemie_Obj.at(itEnnemie)->getY())->set_image(Ennemie_Obj.at(itEnnemie)->getImage());
		}

		//D�placer Ennemie
		for (int itEnnemie = 0; itEnnemie < Ennemie_Obj.size(); itEnnemie++)
		{
			Ennemie* ennemieTemp = Ennemie_Obj.at(itEnnemie);
			srand(itEnnemie * time(0));
			int randMove = rand() % 2;
			if (randMove)
			{
				ennemieTemp->Move(0,-1);
				if (ennemieTemp->getY() == 1)
				{
					delete(ennemieTemp);
					Ennemie_Obj.erase(Ennemie_Obj.begin() + itEnnemie);
				}
			}
		}


		//Ennemie Attack
		for (int itEnnemie = 0; itEnnemie < Ennemie_Obj.size(); itEnnemie++)
		{
			Ennemie* ennemieTemp = Ennemie_Obj.at(itEnnemie);
			srand(itEnnemie * time(0));
			int randAttack = rand() % 2;
			if (randAttack)
			{
				ProectileEnnemies.push_back(ennemieTemp->attack(ennemieTemp->getX(), ennemieTemp->getY()));
			}		
		}

		//Afficher les projectiles des Ennemies

		for (int iterator = 0; iterator < ProectileEnnemies.size(); iterator++)
		{
			Projectile* projectileTemp = ProectileEnnemies.at(iterator);

			projectileTemp->Move(0, -1);
			if (projectileTemp->getY() ==  1)
			{
				delete(projectileTemp);
				ProectileEnnemies.erase(ProectileEnnemies.begin() + iterator);
			}
			else
			{
				grilleDeJeu.getCase(projectileTemp->getX(), projectileTemp->getY())->set_image(projectileTemp->getImage());
			}
		}

		//Afficher les projectiles du joueur
		for (int iterator = 0; iterator < ProjectileJoueur.size(); iterator++)
		{
			ProjectileJoueur.at(iterator)->Move(0, 1);
			if (ProjectileJoueur.at(iterator)->getY() == grilleDeJeu.getHauteur() - 1)
			{
				delete(ProjectileJoueur.at(iterator));
				ProjectileJoueur.erase(ProjectileJoueur.begin() + iterator);
			}
			else
			{
				grilleDeJeu.getCase(ProjectileJoueur.at(iterator)->getX(), ProjectileJoueur.at(iterator)->getY())->set_image(ProjectileJoueur.at(iterator)->getImage());
			}
		}

		grilleDeJeu.Afficher();

		switch ((c = _getch())) {
		case KEY_LEFT:
			if(joueur_obj.getX() < grilleDeJeu.getLargeur() -2)
				joueur_obj.Move(1, 0);
			break;
		case KEY_RIGHT:
			if (joueur_obj.getX() > 1)
				joueur_obj.Move(-1, 0);
			break;
		case KEY_UP:
			ProjectileJoueur.push_back(joueur_obj.attack(joueur_obj.getX(), joueur_obj.getY()));
			break;
		default:
			break;
		}

	}
    return 0;
}

