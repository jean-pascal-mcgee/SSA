#pragma once
#include <vector>
#include <stdio.h>
#include <iostream>

using namespace std;

class Case {
private:
	char image;
public:
	Case();
	Case(char imageInit);
	~Case();
	char get_image();
	void set_image(char nouvelleImage);
	bool estVide();
};

class Grille {
private:
	int largeur;
	int hauteur;
	vector<vector<Case>> grilleDeCase;
	void bordure();
public:
	Grille(int hauteur, int largeur);
	~Grille();
	void Afficher();
	void viderGrille();
	int getLargeur();
	int getHauteur();
	Case* getCase(int x, int y);
};

class Projectile {
private:
	int posX, posY;
	char Image;
	int puissance;
public:
	Projectile();
	Projectile(int x, int y, char image, int puissance);
	void Move(int x, int y);
	int getX();
	int getY();
	char getImage();
	void setPuissance(int puissance);
	int getPuissance();
};

class Vaisseau {
private:
	int Hp;
	char Image;
	int x, y;
public:
	Vaisseau();
	Vaisseau(int hpInit, char imageInit, int xI, int yI);
	void Move(int xM, int yM);

	int getHp();
	void setHp(int hp);

	char getImage();
	void setImage(char image);

	int getX();
	void setX(int x);

	int getY();
	void setY(int y);
};

class Ennemie : public Vaisseau {
private:
	int points;
public:
	Ennemie();
	Ennemie(int hp, char image, int x, int y,int points);
	Projectile* attack(int x, int y);

};

class Joueur : public Vaisseau {
private:
	int level;
public:
	Joueur();
	Joueur(int hp, char image, int x , int y , int level);
	Projectile* attack(int x, int y);
};

