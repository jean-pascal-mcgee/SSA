/**
* @file Objets.cpp
* @Author Équipe P16
* @date Fevrier, 2016
* @brief Fichier cpp qui contient l'implémentation de certaines classe utile au projet
*/

#include "Objets.h"


void Grille::bordure()
{
	//Ajouter une bordure
	for (int x = 0; x < largeur; x++)
	{
		grilleDeCase[0][x].set_image('=');
		grilleDeCase[hauteur - 1][x].set_image('=');
	}
	for (int y = 0; y < hauteur; y++)
	{
		grilleDeCase[y][0].set_image('|');
		grilleDeCase[y][largeur - 1].set_image('|');
	}
}

/**
*	@name Grille
*	@brief Implémentation des méthodes de Grille
*/
Grille::Grille(int hauteurIn,int largeurIn)
{
	this->hauteur = hauteurIn;
	this->largeur = largeurIn;

	//Établir la taille dee nos vectors (hauteur x largeur )
	this->grilleDeCase.resize(hauteur);
	for (int i = 0; i < hauteur; i++)
	{
		grilleDeCase[i].resize(largeur);
	}
	this->bordure();
}

Grille::~Grille()
{
}

void Grille::Afficher()
{
	for (int y = hauteur-1; y >= 0; y--) 
	{
		for (int x = largeur-1; x >= 0; x--)
		{
			cout << grilleDeCase[y][x].get_image();
		}
		cout << "\n";
	}
}

void Grille::viderGrille()
{
	for (int i = 0; i < hauteur; i++)
	{
		for (int it = 0; it < largeur; it++)
		{
			grilleDeCase[i][it] = ' ';
		}
	}
	this->bordure();
}

int Grille::getLargeur()
{
	return this->largeur;
}

int Grille::getHauteur()
{
	return this->hauteur;
}

Case * Grille::getCase(int x, int y)
{
	return &this->grilleDeCase[y][x];
}

/**
*	@name Case
*	@brief Implémentation des méthodes de Case
*/
Case::Case()
{
	this->image = ' ';
}

Case::Case(char imageInit)
{
	this->image = imageInit;
}

Case::~Case()
{
}

char Case::get_image()
{
	return this->image;
}

void Case::set_image(char nouvelleImage)
{
	this->image = nouvelleImage;
}

bool Case::estVide()
{
	if (this->image == ' ')
		return true;
	else
		return false;
}

/**
*	@name Vaisseau
*	@brief Implémentation des méthodes de Vaisseau
*/
Vaisseau::Vaisseau()
{
	this->Image = ' ';
	this->Hp = 1;
	this->x = 1;
	this->y = 1;
}

Vaisseau::Vaisseau(int hpInit, char imageInit,int xI,int yI)
{
	this->Image = imageInit;
	this->Hp = hpInit;
	this->x = xI;
	this->y = yI;
}

void Vaisseau::Move(int xM, int yM)
{
	this->x = this->x + xM;
	this->y = this->y + yM;
}

int Vaisseau::getHp()
{
	return this->Hp;
}

void Vaisseau::setHp(int hp)
{
	this->Hp = hp;
}

char Vaisseau::getImage()
{
	return this->Image;
}

void Vaisseau::setImage(char image)
{
	Image = image;
}

int Vaisseau::getX()
{
	return this->x;
}

void Vaisseau::setX(int x)
{
	this->x = x;
}

int Vaisseau::getY()
{
	return this->y;
}

void Vaisseau::setY(int y)
{
	this->y = y;
}



Joueur::Joueur() : Vaisseau()
{
	this->setHp(0);
	this->setImage(' ');
	this->setX(0);
	this->setY(0);
	this->level = 1;
}

Joueur::Joueur(int hp, char image, int x, int y, int level)
{
	this->setHp(hp);
	this->setImage(image);
	this->setX(x);
	this->setY(y);
	this->level = level;
}

Projectile* Joueur::attack(int x, int y)
{
	return new Projectile(x,y,'|',1);
}

Projectile::Projectile()
{
	this->posX = 0;
	this->posY = 0;
	this->Image = ' ';
	this->puissance = 0;
}

Projectile::Projectile(int x, int y, char image, int puissance)
{
	this->posX = x;
	this->posY =y;
	this->Image = image;
	this->puissance = puissance;
}

void Projectile::Move(int x, int y)
{
	this->posX = this->posX + x;
	this->posY = this->posY + y;
}

int Projectile::getX()
{
	return this->posX;
}

int Projectile::getY()
{
	return this->posY;
}

char Projectile::getImage()
{
	return this->Image;
}

void Projectile::setPuissance(int puissance)
{
	this->puissance = puissance;
}

int Projectile::getPuissance()
{
	return this->puissance;
}

Ennemie::Ennemie() : Vaisseau()
{
	this->points = 0;
}

Ennemie::Ennemie(int hp, char image, int x, int y, int points) : Vaisseau()
{
	this->setHp(hp);
	this->setImage(image);
	this->setX(x);
	this->setY(y);
	this->points = points;
}

Projectile * Ennemie::attack(int x, int y)
{
	return new Projectile(x, y, '|', 1);
}
